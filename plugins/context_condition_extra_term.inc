<?php

/**
 * Expose taxonomy terms path as a context condition.
 */
class context_condition_extra_term extends context_condition_node_taxonomy {
    function options_form($context) {
        return array();
    }

    function execute($term, $op) {
        foreach ($this->get_contexts($term->tid) as $context) {
                $this->condition_met($context, $term->tid);
        }
    }
}
