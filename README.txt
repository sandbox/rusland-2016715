Context Extra Terms
==================

This module works with Drupal 7.x and Context 3.x

Context Extra Terms provides a context condition reactive to current
taxonomy view.

Installation
------------

1. Put the module in 'sites/all/modules'

2. Enable the module on the 'admin/build/modules' page.


Maintainers
-----------

- Rusland (Ruslan Rindevich)
